package doorcode;

import java.util.HashSet;

/**
 *
 * @author Lasse Lybeck
 */
public class DoorCodeNodeShort implements IDoorCodeNode {

    private HashSet<Short> missingCodes;
    private String code;
    private int digitsInCode;
    private int radix;

    public DoorCodeNodeShort(int codeLength, int radix) {
        if (codeLength < 1) {
            throw new IllegalArgumentException("Code length must be positive! Was: " + codeLength);
        }
        if (radix < 1) {
            throw new IllegalArgumentException("Number of digits must be positive! Was: " + radix);
        }
        if (radix > 10) {
            throw new IllegalArgumentException("At most 10 digits supported! Was: " + radix);
        }
        this.digitsInCode = codeLength;
        this.radix = radix;
        this.code = "";
        this.missingCodes = new HashSet<>((int) Math.pow(radix, codeLength));
        generateCodes();
    }

    public DoorCodeNodeShort(IDoorCodeNode o) {
        if (!(o instanceof DoorCodeNodeShort)) {
            throw new IllegalArgumentException("Cannot copy instance of type " + o.getClass().getName());
        }
        DoorCodeNodeShort d = (DoorCodeNodeShort) o;
        this.missingCodes = new HashSet<>(d.missingCodes);
        this.code = d.getCode();
        this.digitsInCode = d.getDigitsInCode();
        this.radix = d.getDigitsUsed();
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public int getCodeLength() {
        return code.length();
    }

    @Override
    public int getDigitsInCode() {
        return digitsInCode;
    }

    @Override
    public int getDigitsUsed() {
        return radix;
    }

    @Override
    public int getCodesLeft() {
        return missingCodes.size();
    }

    @Override
    public void addNumber(int i) {
        if (i < 0 || i > radix) 
            throw new IllegalArgumentException("Numbers must be between 0 and (maximum digits - 1)! Was: " + i + ", when max digits = " + radix);
        code += i;
        if (code.length() >= digitsInCode) {
            short newCode = Short.parseShort(code.substring(code.length() - digitsInCode), radix);
            missingCodes.remove(newCode);
        }
    }

    private void generateCodes() {
        int max = (int) Math.pow(radix, digitsInCode);
        for (short i = 0; i < max; i++) {
            missingCodes.add(i);
        }
    }
}
