package doorcode;

import java.util.Set;

/**
 *
 * @author Lasse Lybeck
 */
public interface IDoorCodeNode {

    String getCode();
    
    int getCodeLength();

    int getDigitsInCode();
    
    int getDigitsUsed();

    int getCodesLeft();

    void addNumber(int i);
}
