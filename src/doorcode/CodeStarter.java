package doorcode;

/**
 *
 * @author Lasse Lybeck
 */
public class CodeStarter {

    private static final String INFO = "Usage: java -jar Doorcode.jar [code length] [number of digits]";

    public static void main(String[] args) {

        if (args.length == 0 || args.length > 2) {
            System.out.println(INFO);
            return;
        }

        int codeLength, digits;
        try {
            codeLength = Integer.parseInt(args[0]);
            digits = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            System.out.println("Could not parse arguments to integers...");
            System.out.println(INFO);
            return;
        }
        if (codeLength < 1) {
            System.out.println("Code length must be positive. Was: " + codeLength);
            System.out.println(INFO);
            return;
        }
        if (digits < 1) {
            System.out.println("Number of digits must be positive. Was: " + digits);
            System.out.println(INFO);
            return;
        }
        
//        int codeLength = 3;
//        int digits = 2;

        CodeSolver codeSolver = new CodeSolver(codeLength, digits);
        Timer timer = new Timer();

        timer.tic();
        String code = codeSolver.getShortestString();
        double time = timer.toc();

        System.out.println();
        System.out.println("Code length: " + codeLength);
        System.out.println("Number of digits: " + digits);
        System.out.println("Time elapsed: " + time + " seconds.");
        System.out.println("Length of code: " + code.length());
        System.out.println();
        System.out.println("Shortest code:");
        System.out.println(code);
        System.out.println();
    }
}
