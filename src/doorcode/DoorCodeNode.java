package doorcode;

import java.util.HashSet;

public class DoorCodeNode implements IDoorCodeNode {

    private HashSet<String> missingCodes;
    private String code;
    private final int digitsInCode;
    private final int digitsUsed;

    public DoorCodeNode(int codeLength, int digits) {
        if (codeLength < 1) {
            throw new IllegalArgumentException("Code length must be positive! Was: " + codeLength);
        }
        if (digits < 1) {
            throw new IllegalArgumentException("Number of digits must be positive! Was: " + digits);
        }
        if (digits > 10) {
            throw new IllegalArgumentException("At most 10 digits supported! Was: " + digits);
        }
        this.digitsInCode = codeLength;
        this.digitsUsed = digits;
        this.code = "";
        this.missingCodes = new HashSet<>((int) Math.pow(digits, codeLength));
        generateCodes();
    }

    public DoorCodeNode(IDoorCodeNode o) {
        if (!(o instanceof DoorCodeNode)) {
            throw new IllegalArgumentException("Cannot copy instance of type " + o.getClass().getName());
        }
        DoorCodeNode d = (DoorCodeNode) o;
        this.missingCodes = new HashSet<>(d.missingCodes);
        this.code = d.getCode();
        this.digitsInCode = d.getDigitsInCode();
        this.digitsUsed = d.getDigitsUsed();
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public int getCodeLength() {
        return code.length();
    }

    @Override
    public int getCodesLeft() {
        return missingCodes.size();
    }

    @Override
    public int getDigitsInCode() {
        return digitsInCode;
    }

    @Override
    public int getDigitsUsed() {
        return digitsUsed;
    }

    @Override
    public void addNumber(int i) {
        if (i < 0 || i >= digitsUsed) {
            throw new IllegalArgumentException("Numbers must be between 0 and (maximum digits - 1)! Was: " + i + ", when max digits = " + digitsUsed);
        }
        code += i;
        if (code.length() >= digitsInCode) {
            String newCode = code.substring(code.length() - digitsInCode);
            missingCodes.remove(newCode);
        }
    }

    private void generateCodes() {
        generateCodes("");
    }

    private void generateCodes(String string) {
        if (string.length() == digitsInCode) {
            missingCodes.add(string);
            return;
        }
        for (int i = 0; i < digitsUsed; i++) {
            generateCodes(string + i);
        }
    }

    @Override
    public String toString() {
        return code;
    }
}
