package doorcode;

/**
 *
 * @author Lasse Lybeck
 */
public class Timer {

    long startTime;

    public void tic() {
        startTime = System.currentTimeMillis();
    }

    public double toc() {
        long endTime = System.currentTimeMillis();
        return 1.0 * (endTime - startTime) / 1000;
    }
}
