package doorcode;

import java.util.Comparator;

/**
 *
 * @author Lasse Lybeck
 */
public class DoorCodeComparator implements Comparator<IDoorCodeNode> {

    @Override
    public int compare(IDoorCodeNode t, IDoorCodeNode t1) {
        int i = t.getCodeLength() + t.getCodesLeft() - t1.getCodeLength() - t1.getCodesLeft();
        if (i == 0) {
            return t.getCodesLeft() - t1.getCodesLeft();
        }
        return i;
    }

}
