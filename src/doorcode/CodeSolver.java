package doorcode;

import java.util.PriorityQueue;

/**
 *
 * @author Lasse Lybeck
 */
public class CodeSolver {

    private final int codeLength;
    private final int digits;

    public CodeSolver(int codeLength, int digits) {
        this.codeLength = codeLength;
        this.digits = digits;
    }

    public String getShortestString() {
        PriorityQueue<IDoorCodeNode> q = new PriorityQueue<>(1024, new DoorCodeComparator());
        q.add(new DoorCodeNode(codeLength, digits));
        int len = 0;
        while (!q.isEmpty()) {
            IDoorCodeNode node = q.remove();
            int newlen = node.getCodeLength();
            if (newlen > len) {
                len = newlen;
//                System.out.println(len);
            }
            for (int i = 0; i < digits; i++) {
                IDoorCodeNode newNode = new DoorCodeNode(node);
                newNode.addNumber(i);
                if (newNode.getCodesLeft() == 0) {
                    return newNode.getCode();
                }
                q.add(newNode);
            }
        }

        return null;
    }
}
